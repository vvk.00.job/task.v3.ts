import { computed, ref, type Ref } from "vue";
import { Filter } from "../types/Filters";
import { Todo } from "../types/Todo";


export const useFilter = (todo:Ref<Todo[]>) => {
  const activeFilter = ref<Filter>("all");
  const setFilter = (filter: Filter) => {
    activeFilter.value = filter;
  };

  const filteredTodos = computed(() => {
    switch (activeFilter.value) {
      case "active":
        return activeTodos.value;
      case "Done":
        return doneTodos.value;
      case "all":
      default:
        return todo.value;
    }
  });

  const activeTodos = computed(() => todo.value.filter((item:Todo) => !item.completed));
  const doneTodos = computed(() => todo.value.filter((item:Todo) => item.completed));

  return {
    activeFilter,
    filteredTodos,
    setFilter,
    activeTodos,
    doneTodos,
  };
};
