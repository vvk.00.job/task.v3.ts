import { onMounted, ref } from "vue";
import { Todo } from "../types/Todo";
import axios from "axios";

export const useTask = () => {
  const todo = ref<Todo[]>([]);

  const addTask = (task: Todo) => {
    todo.value.push(task);
  };
  const getTasks = async () => {
    const { data } = await axios<Todo[]>("https://jsonplaceholder.typicode.com/todos/?_limit=10");
    todo.value = data;
  };

  const remove = (id: number) => {
    todo.value = todo.value.filter((item) => item.id !== id);
  };

  const toggleTodo = (id: number) => {
    const targetTodo = todo.value.find((item: Todo) => item.id === id);
    if (targetTodo) {
      targetTodo.completed = !targetTodo.completed;
    }
  };

  onMounted(() => {
    getTasks();
  });

  return {
    todo,
    addTask,
    remove,
    toggleTodo,
  };
};
